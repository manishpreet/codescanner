import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

class Util {}

toast({String message, BuildContext context}) {
  Toast.show(message, context);
}
