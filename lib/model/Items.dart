class Items {
  List<Item> items = List();

  List<Item> getItems() {
    items.add(Item(image: 'assest/barcode-scanner.png', name: 'Live Scan'));
    items.add(Item(image: 'assest/gallery.png', name: 'Pick from Gallery'));
    items.add(Item(image: 'assest/create.png', name: 'Create Barcode'));
    items.add(Item(image: 'assest/settings.png', name: 'About'));

    return items;
  }
}

class Item {
  String image;
  String name;

  Item({this.image, this.name}) {}
}
