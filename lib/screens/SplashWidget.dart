import 'dart:async';

import 'package:codescanner/screens/HomeWidget.dart';
import 'package:flutter/material.dart';

class SplashWidget extends StatefulWidget {
  @override
  _SplashWidgetState createState() => _SplashWidgetState();
}

class _SplashWidgetState extends State<SplashWidget> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topRight,
                end: Alignment.bottomLeft,
                colors: [Colors.deepOrange, Colors.amber])
          ),
          child: Center(
            child:
                InkWell(
                  onTap: ()=>setTimer(),
                  child: Image.asset('assest/finger.png',
                    height: 150,
                  width: 150,),
                ),
          ),


      ),
    );
  }

  @override
  void initState() {
    setTimer();
  }

  void setTimer() {
    Timer(Duration(seconds: 4), () {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => HomeWidget()));
    });
  }
}
