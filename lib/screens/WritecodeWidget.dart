import 'package:codescanner/screens/ShowBarcodeWidget.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

TextEditingController code = TextEditingController();

class WritecodeWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Code Scanner'),
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: TextField(
                controller: code,
                decoration: new InputDecoration.collapsed(
                    hintText: 'Write text here to generate Qrcode'),
              ),
            ),
          ),
          InkWell(
            onTap: () {
              String text = code.text.toString();
              if (text.isNotEmpty)
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => ShowBarcodeWidget(text)));
              else
                Toast.show("Enter Text To Generate Qrcode", context);
            },
            child: Container(
              padding: EdgeInsets.all(16.0),
              decoration: BoxDecoration(color: Colors.amber),
              width: MediaQuery.of(context).size.width,
              // padding: EdgeInsets.all(16.0),
              child: Text(
                'Generate Qrcode',
                style: TextStyle(fontSize: 18.0),
                textAlign: TextAlign.center,
              ),
            ),
          )
        ],
      ),
    );
  }
}
