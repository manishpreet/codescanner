import 'package:flutter/material.dart';

class AboutWidget extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Code Scanner'),
      backgroundColor: Colors.amber,),
      body: Center(
        child: Container(
          margin: EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Image.asset('assest/finger.png',
                height: 150,
                width: 150,),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text('You Can Scan Live Barcode ,Scan Screenshot, You can also create own Barcode with own hidden information, for any feedBack  drop mail at...',
                style: TextStyle(
                  letterSpacing: 0.8
                ),),

              ), Text('manishpreet0786@gmail.com',
              style: TextStyle(
                fontSize: 18.0,
                color: Colors.amber
              ),)
            ],
          ),
        ),
      ),
    );
  }
}
