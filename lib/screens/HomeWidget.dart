import 'package:codescanner/model/Items.dart';
import 'package:codescanner/model/Util.dart';
import 'package:codescanner/screens/AboutWidget.dart';
import 'package:codescanner/screens/ScannerWidget.dart';
import 'package:codescanner/screens/WritecodeWidget.dart';
import 'package:flutter/material.dart';
import 'package:path/path.dart';
import 'package:qr_code_tools/qr_code_tools.dart';
import 'package:image_picker/image_picker.dart';

class HomeWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Code Scanner',
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Center(
          child: GridView.count(
              crossAxisCount: 2,
              mainAxisSpacing: 8.0,
              crossAxisSpacing: 8.0,
              children: Items()
                  .getItems()
                  .map((item) => Card(
                        child: ItemWidget(item),
                      ))
                  .toList()),
        ),
      ),
    );
  }
}

class ItemWidget extends StatelessWidget {
  Item item;
  ItemWidget(this.item);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => filterClick(item, context),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Image.asset(
            item.image,
            height: MediaQuery.of(context).size.height / 5,
            width: MediaQuery.of(context).size.width / 5,
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 16.0),
            child: Text(item.name),
          )
        ],
      ),
    );
  }

  filterClick(Item item, BuildContext context) {
    switch (item.name) {
      case 'Live Scan':
        {
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (cotext) => HomePage(scanner: true,)));
          break;
        }
      case 'Pick from Gallery':
        {
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (cotext) => HomePage()));

          //getImage(context);
          break;
        }
      case 'Create Barcode':
        {
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (cotext) => WritecodeWidget()));

          break;
        }
      case 'About':
        {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => AboutWidget()));
          break;
        }
    }
  }

  Future getImage(BuildContext context) async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);

    String data = await QrCodeToolsPlugin.decodeFrom(image.path);
    toast(message:data,context: context);
  }
}
