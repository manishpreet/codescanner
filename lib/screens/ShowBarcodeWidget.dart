import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';
import 'dart:ui' as ui;
import 'package:codescanner/model/Util.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:path_provider/path_provider.dart';
import 'package:esys_flutter_share/esys_flutter_share.dart';

class ShowBarcodeWidget extends StatelessWidget {
  String text;
  ShowBarcodeWidget(this.text);
  GlobalKey _globalKey = new GlobalKey();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
        appBar: AppBar(
          title: Text('Code Scanner'),
        ),
        body: Container(
          margin: EdgeInsets.only(top: 50.0),
          child: Center(
            child: Column(
              children: <Widget>[
                RepaintBoundary(
                  key: _globalKey,
                  child: QrImage(
                    data: text,
                    size: 200.0,
                  ),
                ),
                _showNeedHelpButton(() {
                  _capturePng(context);
                })
              ],
            ),
          ),
        ));
  }

  Widget _showNeedHelpButton(Function listner) {
    return Padding(
      padding: EdgeInsets.only(top: 50),
      child: Material(
        //Wrap with Material
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(22.0)),
        elevation: 4.0,
        color: Colors.amber,
        clipBehavior: Clip.antiAlias, // Add This
        child: MaterialButton(
          minWidth: 200.0,
          height: 35,
          child: new Text('continue to share',
              style: new TextStyle(fontSize: 16.0, color: Colors.white)),
          onPressed: () {
            listner();
          },
        ),
      ),
    );
  }

  Future<Uint8List> _capturePng(context) async {
    try {
      RenderRepaintBoundary boundary =
          _globalKey.currentContext.findRenderObject();
      ui.Image image = await boundary.toImage(pixelRatio: 3.0);
      ByteData byteData =
          await image.toByteData(format: ui.ImageByteFormat.png);
      var pngBytes = byteData.buffer.asUint8List();
      var bs64 = base64Encode(pngBytes);
      print(pngBytes);
      print(bs64);
      await Share.file('esys image', 'esys.png', pngBytes, 'image/png', text: 'Code Scanner share this qr code for you....');
      //downloadImage(pngBytes, context);
      return pngBytes;
    } catch (e) {
      print(e);
    }
  }

  Future<dynamic> downloadImage(bytes, context) async {
    String dir = (await getTemporaryDirectory()).path;
    File file = new File('$dir/${DateTime.now()}.png');

    if (file.existsSync()) {
      toast(message: 'file already exist', context: context);
      var image = await file.readAsBytes();
      return image;
    } else {
      await file.writeAsBytes(bytes);
      toast(message: file.path, context: context);
      return bytes;
    }
  }
}
