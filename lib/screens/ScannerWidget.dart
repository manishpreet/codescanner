import 'dart:async';

import 'package:codescanner/model/Util.dart';
import 'package:flutter/material.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:qr_code_tools/qr_code_tools.dart';

class HomePage extends StatefulWidget {

  bool isScanner=false;
  HomePage({scanner=false})
  {
    isScanner=scanner;
  }
  @override
  ScannerWidget createState() {
    return new ScannerWidget();
  }
}

class ScannerWidget extends State<HomePage> {
  String result = "Hey there !";

  Future _scanQR() async {
 
       try {
         if(widget.isScanner)
           {
             String qrResult = await BarcodeScanner.scan();
             setState(() {
               result = qrResult;
             });
           }else
             {
               var image = await ImagePicker.pickImage(source: ImageSource.gallery);

               String data = await QrCodeToolsPlugin.decodeFrom(image.path);
               setState(() {
                 result = data;
               });
             }

       } on PlatformException catch (ex) {
         if (ex.code == BarcodeScanner.CameraAccessDenied) {
           setState(() {
             result = "Camera permission was denied";
           });
         } else {
           setState(() {
             result = "Unknown Error $ex";
           });
         }
       } on FormatException {
         setState(() {
           result = "You pressed the back button before scanning anything";
         });
       } catch (ex) {
         setState(() {
           result = "Unknown Error $ex";
         });
       }


  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Code Scanner"),
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Text(
            result,
            style: new TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        icon: Icon(widget.isScanner ? Icons.camera_alt :Icons.image),
        label: Text(widget.isScanner ? "Scan" : 'open Gallery'),
        onPressed: _scanQR,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}
